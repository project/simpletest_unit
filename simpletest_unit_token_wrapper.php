<?php

define('T_NONE', -1);

class simpletest_unit_token_wrapper {
  private $tokens = array();
  private $file_path;
  private $functions = array();
  private $function_names = array();

  public function simpletest_unit_token_wrapper($contents, $file_path = NULL) {
    $this->tokens = token_get_all($contents);
    $this->file_path = $file_path;
  }

  public function get_tokens() {
    return $this->tokens;
  }

  public function get_file_path() {
    return $this->file_path;
  }

  public function find_functions() {
    reset($this->tokens);
    while (next($this->tokens) !== FALSE) {
      if ($this->is_type(T_FUNCTION)) {
        $function = $this->get_function_info();
        $this->functions[] = $function;
        $this->function_names[] = $function['name'];
      }
    }
    return $this->functions;
  }

  public function get_functions() {
    return $this->functions;
  }

  public function get_function_names() {
    return $this->function_names;
  }

  public function clean_calls(&$function_names) {
    foreach ($this->functions as &$function) {
      $count = count($function['calls']);
      for ($i = 0; $i < $count; $i++) {
        if (!in_array($function['calls'][$i], $function_names)) {
          unset($function['calls'][$i]);
        }
      }
    }
  }

  private function get_function_info() {
    $function = array();
    $function['parameters'] = array();
    $function['calls'] = array();
    $block_depth = NULL;
    $current_paramter = '';
    $open_call = FALSE;
    $open_array = FALSE;
    $reference = FALSE;

    while (next($this->tokens) !== FALSE && $block_depth !== 0) {
      if (empty($function['name'])) {
        // If function name hasn't been set then look for first string.
        if ($this->is_type(T_STRING)) {
          $function['name'] = $this->value();
          $current_paramter = '';
        }
      }
      else if ($block_depth === NULL) {
        // If function body hasn't started than look for parameters
        switch ($this->type()) {
          case T_VARIABLE:
            $function['parameters'][$current_paramter = ($reference ? '&' : '') . $this->value()] = NULL;
            $reference = FALSE;
            break;
          case T_STRING:
          case T_CONSTANT_ENCAPSED_STRING:
          case T_LNUMBER:
          case T_DNUMBER:
            $function['parameters'][$current_paramter] = $this->value();
            break;
          case T_NONE:
            if ($this->value() == '{') {
              $block_depth = 1;
            }
            else if ($this->value() == '&') {
              $reference = TRUE;
            }
            break;
        }
      }
      else {
        // Look for internal function calls.
        switch ($this->type()) {
          case T_STRING:
            // Possible function call, check to see if it is drupal function.
            if (!in_array($open_call = $this->value(), $function['calls'])) {
              $function['calls'][] = $open_call;
            }
            break;
          case T_NONE:
            if ($open_call && $this->value() == ')') {
              $open_call = FALSE;
            }
            else if ($this->value() == '{') {
              $block_depth++;
            }
            else if ($this->value() == '}') {
              $block_depth--;
            }
            break;
        }
      }
    }
    return $function;
  }

  private function is_type($type) {
    $token = current($this->tokens);
    if (is_array($token)) {
      return $token[0] == $type;
    }
    return FALSE;
  }

  private function type() {
    $token = current($this->tokens);
    if (is_array($token)) {
      return $token[0];
    }
    return T_NONE;
  }

  private function value() {
    $token = current($this->tokens);
    if (is_array($token)) {
      return $token[1];
    }
    return $token;
  }

  public function clear_tokens() {
    $this->tokens = array();
  }

  public function print_tokens() {
    foreach ($this->tokens as $token) {
      if (is_array($token)) {
        print(token_name($token[0]) .': '. $token[1] ."\n");
      }
      else {
        print($token ."\n");
      }
    }
  }
}
